# Void Linux Install Checklist 

A checklist I have created for installing Void Linux 

- [ ] [Download](voidlinux.org/download) the X86_64 base Live Image [glibc] 

- [ ] Create a bootable USB 

- [ ] Boot from USB 

- [ ] Follow on screen instructions 

- [ ] Partition disks with fdisk (see my wiki if forgot how to use fdisk) 

- [ ] Reboot system and login 
